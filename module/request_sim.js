function dump(obj)
	{
		var out = '';
		for (var i in obj) {out += i + "= " + obj[i] + "\n";}
		console.log(out);
	};

var sim_state = function (url,callback,methode_to_c)
	{
		var options = {'uri': url,'method': 'GET',};
		var tab = new Object();	

		request=require('request');
		request(options, function (err, response, body)
			{
				var data_json=body.toString();
				data_json = data_json.replace("{","");
				data_json = data_json.replace("}", "");
				data_json=data_json.replace(/"/g, "");
				data_json=data_json.replace(/ /g, "_");
				if (methode_to_c=="none")
					{
						
						data_json=data_json.replace(/\n/g, "");
						data_json=data_json.replace(/\r/g, "");
						//data_json=data_json.replace(/"/g, "");
						data_json=data_json.replace(/ /g, "_");
						
						data_json = data_json.split(",");

						for (var i = 0, len = data_json.length; i < len; i++)
							{
								var temp=data_json[i].split(":");
								var key=temp[0];
								var value=temp[1];
								value=value.replace(".",",");
								tab[key.trim()]=value.trim();
							}

						dump(tab);
						callback({'tts': "informations affichées" });
					}
				else {callback({'tts': data_json});}
				
				
			});

	};

var sim_req_full = function (url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat,callback)
	{
		function wait(ms)
			{
				var d = new Date();
				var d2 = null;

				do { d2 = new Date(); } 
				while(d2-d < ms);
			}

		function convert(json)
			{
				var tab = new Object();	
				var data_json=json.toString();

				
				data_json = data_json.replace("{","");
				data_json = data_json.replace("}", "");
				data_json=data_json.replace(/"/g, "");
				data_json=data_json.replace(/\n/g, "");
				data_json=data_json.replace(/\r/g, "");
				data_json=data_json.replace(/"/g, "");
				data_json=data_json.replace(/ /g, "_");
						
				data_json = data_json.split(",");
				
				for (var i = 0, len = data_json.length; i < len; i++)
					{
					  	var temp=data_json[i].split(":");
						var key=temp[0];
						var value=temp[1];
						value=value.replace(".",",");
						tab[key.trim()]=value.trim();
					}
				return tab;
			}

		var options2 = {'uri': url2,'method': 'GET',};
		
		if (parametre=='none')
			{
				var options ={
									'uri': url,
									'method': 'POST',
									'Headers' : 'Content-type: application/json',
									'json': {"cmd": commande},
								};
			}
		else
			{
				var options = 	{
							'uri': url,
							'method': 'POST',
							'Headers' : 'Content-type: application/json',
							'json': {"cmd": commande , "param": parametre},
						};
			}
		
		request1=require('request');
		request1(options2, function (err, response, body)
			{
				
				//console.log(body);
				if (body!=undefined)
					{
						var data_req1 = convert(body);
						if (data_req1[callback_commande]==etat) {callback({'tts': encore});}
						else
							{
								request2=require('request');
								request2(options, function (err, response, body)
									{
										if (!err && response.statusCode == 200)
											{
												wait(1250);
												request3=require('request');
												request3(options2, function (err, response, body)
													{
														var data_req2 = convert(body);
														if (body!=undefined)
															{
																console.log(data_req2[callback_commande]+"="+etat);
																if (data_req2[callback_commande]!=etat) {callback({'tts': echec});}
																else {callback({'tts': ok});}
															}
														else
															{
																callback({'tts': pas_ok});
															}
													});
											}
										else
											{
												callback({'tts': pas_ok });
											}
									});
							}
					}
				else
					{
						callback({'tts': pas_ok});
					}
			});
		};
var sim_req_bind = function (url,parametre,commande,ok,pas_ok,callback)
	{
		if (parametre=='none')
			{
				var options ={
									'uri': url,
									'method': 'POST',
									'Headers' : 'Content-type: application/json',
									'json': {"cmd": commande},
								};
			}
		else
			{
				var options = 	{
							'uri': url,
							'method': 'POST',
							'Headers' : 'Content-type: application/json',
							'json': {"cmd": commande , "param": parametre},
						};
			}
		
		request=require('request');
		request(options, function (err, response, body)
			{
				if (!err && response.statusCode == 200)
					{
						callback({'tts': ok });
					}	
				else
					{
						callback({'tts': pas_ok });
					}
			});
							
	};

exports.sim_state = sim_state;
exports.sim_req_full = sim_req_full;
exports.sim_req_bind = sim_req_bind;
