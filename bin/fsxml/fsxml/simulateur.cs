﻿using System;
using System.Threading;
using Microsoft.FlightSimulator.SimConnect;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Timers;

namespace fsxml
{
    /// <summary>
    /// Structure destinée à recevoir les données du simulateur lors s'une requete vers simconnect
    /// </summary>

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct DonneesAvion
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 0x100)]
        public string Title;
        //Position
        public double Plane_Latitude;
        public double Plane_Longitude;
        public double Plane_Altitude;
        public double Airspeed_Indicated;
        public double Plane_Heading_Degrees_Magnetic;
        public double Plane_Heading_Degrees_True;
        //Radios
        public double Com_Active_Frequency_1;
        public double Com_Active_Frequency_2;
        public double Com_Standby_Frequency_1;
        public double Com_Standby_Frequency_2;
        public double Nav_Active_Frequency_1;
        public double Nav_Active_Frequency_2;
        public double Nav_Standby_Frequency_1;
        public double Nav_Standby_Frequency_2;
        //Auto Pilot
        public int Autopilot_Master;
        public int Autopilot_Approach_Hold;

        public int Autopilot_Heading_Lock;
        public double Autopilot_Heading_Lock_Dir;

        public int Autopilot_Altitude_Lock;
        public double Autopilot_Altitude_Lock_Var;
        public double Autopilot_Vertical_Hold_Var;

        public int Autopilot_Throttle_Arm;
        public int Autopilot_Airspeed_Hold;
        public double Autopilot_Airspeed_Hold_Var;

        public int Autopilot_Nav1_Lock;
        public int Gps_Drives_Nav1;

        //Engines
        public int Number_Of_Engines;
        public double General_Eng_Throttle_Lever_Position_1;
        public double General_Eng_Throttle_Lever_Position_2;
        public double General_Eng_Throttle_Lever_Position_3;
        public double General_Eng_Throttle_Lever_Position_4;

        //Other controls
        public double Flaps_Handle_Percent;
        public int Gear_Handle_Position;
        public int Spoilers_Armed;
        public double Spoilers_Handle_Position;
        public double Brake_Left_Position;
        public double Brake_Right_Position;
    }
    /// <summary>
    /// Enumérateur pour les déclenchement d'event lors de la réception d'une commande json
    /// </summary>

    public enum EventEnum
    {
        AUTOPILOT_ON,
        AUTOPILOT_OFF,
        HEADING_BUG_INC,
        HEADING_BUG_DEC,
        AP_ALT_VAR_INC,
        AP_ALT_VAR_DEC,
        AP_ALT_HOLD_ON,
        AP_ALT_HOLD_OFF,
        AP_HDG_HOLD_ON,
        AP_HDG_HOLD_OFF,
        AUTO_THROTTLE_ARM,
        AP_APR_HOLD_ON,
        AP_APR_HOLD_OFF,
        AP_AIRSPEED_ON,
        AP_AIRSPEED_OFF,
        AP_SPD_VAR_INC,
        AP_SPD_VAR_DEC,
        AP_ALT_VAR_SET_ENGLISH,
        AP_VS_VAR_SET_ENGLISH,
        AP_SPD_VAR_SET,
        HEADING_BUG_SET,
        INC_COWL_FLAPS,
        DEC_COWL_FLAPS,
        FLAPS_UP,
        FLAPS_DOWN,
        FLAPS_1,
        FLAPS_2,
        FLAPS_3,
        FLAPS_SET,
        FLAPS_INCR,
        FLAPS_DECR,
        SPOILERS_ARM_ON,
        SPOILERS_ARM_OFF,
        SPOILERS_ON,
        SPOILERS_OFF,
        GEAR_UP,
        GEAR_DOWN,
        COM_STBY_RADIO_SET,
        COM_STBY_RADIO_SWAP,
        NAV1_STBY_SET,
        NAV1_RADIO_SWAP,
        ENGINE_AUTO_START,
        ENGINE_AUTO_SHUTDOWN,
        VOR1_SET,
        THROTTLE_SET,
        AXIS_LEFT_BRAKE_SET,
        AXIS_RIGHT_BRAKE_SET,
        BRAKES,
        ATC,
        ATC_MENU_1,
        ATC_MENU_2,
        ATC_MENU_3,
        ATC_MENU_4,
        ATC_MENU_5,
        ATC_MENU_6,
        ATC_MENU_7,
        ATC_MENU_8,
        ATC_MENU_9,
        ATC_MENU_0,
        AP_NAV1_HOLD_ON,
        AP_NAV1_HOLD_OFF,
        TOGGLE_GPS_DRIVES_NAV1,
        PARKING_BRAKES
    }

    /// <summary>
    /// Enumération obligatoires pour utiliser simconnect
    /// </summary>
    public enum DATA_REQUESTS
    {
        REQUEST_1
    }
    public enum GROUP
    {
        GROUP1
    }

    public enum DEFINITIONS
    {
        PERIODIQUE,
        INIT
    }

    class simulateur
    { 
        SimConnect m_scConnection;
        EventWaitHandle m_scEvent;
        System.Threading.Thread m_scThread;
        bool m_scThreadRun;
        bool m_bConnected;
        System.Timers.Timer Reco;
        // Conteneur des données reçues de FSX
        public DonneesAvion Donnees;

        public simulateur()
        {
            Donnees = new DonneesAvion();
            m_bConnected = false;
            m_scThread = new Thread(new ThreadStart(ThreadProcedure));
            m_scThread.IsBackground = true;
            m_scEvent = new EventWaitHandle(false, EventResetMode.AutoReset);
            m_scThreadRun = true;
            m_scThread.Start();
        }

        private void Connect ()
        {
            if (!m_bConnected)
            {
                try
                {
                    m_scConnection = new SimConnect("Fsxml", IntPtr.Zero, 0, m_scEvent, 0);
                    m_scConnection.OnRecvOpen += new SimConnect.RecvOpenEventHandler(OnRecvOpen);
                    Reco.Stop();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Erreur de connexion au simulateur, celui ci n'est peut être pas lancé");
                    Console.WriteLine("Nouvel essai dans 10 sec");
                    Reco.Start();
                }
            }
        }

        public bool Transmit (string cmd, uint param)
        {
            if (m_bConnected)
            {
                try
                {
                    m_scConnection.TransmitClientEvent(0, (EventEnum)Enum.Parse(typeof(EventEnum), cmd), param, GROUP.GROUP1, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
                    return false;
                }
                catch (Exception e)
                {
                    Console.WriteLine("La requete simconnect a échouée :" + e.Message);
                    return true;
                }
            }
            else return true;
        }

        private void OnTimer (object source, ElapsedEventArgs e)
        {
            Connect();
        }

        private void ThreadProcedure() // executed by the second thread
        {
            Reco = new System.Timers.Timer();
            Reco.Interval = 10000;
            Reco.Elapsed += new ElapsedEventHandler(OnTimer);
            Connect();
            while (m_scThreadRun)
            {
                m_scEvent.WaitOne(); // wait for an event
                m_scConnection.ReceiveMessage(); // dispatch the event to the correct handler
            }
        }

        private void OnRecvOpen(SimConnect sender, SIMCONNECT_RECV_OPEN data)
        {
            Console.WriteLine("FSXml démarré");
            m_bConnected = true;
            // On programme chaque events sur simconnect avec notre enumérateur
            foreach (EventEnum item in Enum.GetValues(typeof(EventEnum)))
            {
                m_scConnection.MapClientEventToSimEvent(item, item.ToString());
            }
            //On programme la collecte des données FS pour notre structure, en prenant soin de garder le même ordre que la structure
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Title", "", SIMCONNECT_DATATYPE.STRING256, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Plane Latitude", "degrees latitude", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Plane Longitude", "degrees longitude", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Plane Altitude", "feet", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Airspeed Indicated", "knots", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Plane Heading Degrees Magnetic", "degrees", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Plane Heading Degrees True", "degrees", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Com Active Frequency:1", "MHz", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Com Active Frequency:2", "MHz", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Com Standby Frequency:1", "MHz", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Com Standby Frequency:2", "MHz", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Nav Active Frequency:1", "MHz", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Nav Active Frequency:2", "MHz", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Nav Standby Frequency:1", "MHz", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Nav Standby Frequency:2", "MHz", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Autopilot Master", "", SIMCONNECT_DATATYPE.INT32, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Autopilot Approach Hold", "", SIMCONNECT_DATATYPE.INT32, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Autopilot Heading Lock", "", SIMCONNECT_DATATYPE.INT32, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Autopilot Heading Lock Dir", "degrees", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Autopilot Altitude Lock", "", SIMCONNECT_DATATYPE.INT32, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Autopilot Altitude Lock Var", "feet", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Autopilot Vertical Hold Var", "feet/minute", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Autopilot Throttle Arm", "", SIMCONNECT_DATATYPE.INT32, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Autopilot Airspeed Hold", "", SIMCONNECT_DATATYPE.INT32, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Autopilot Airspeed Hold Var", "knots", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Autopilot Nav1 Lock", "", SIMCONNECT_DATATYPE.INT32, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Gps Drives Nav1", "", SIMCONNECT_DATATYPE.INT32, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Number Of Engines", "", SIMCONNECT_DATATYPE.INT32, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "General Eng Throttle Lever Position:1", "", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "General Eng Throttle Lever Position:2", "", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "General Eng Throttle Lever Position:3", "", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "General Eng Throttle Lever Position:4", "", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Flaps Handle Percent", "", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Gear Handle Position", "", SIMCONNECT_DATATYPE.INT32, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Spoilers Armed", "", SIMCONNECT_DATATYPE.INT32, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Spoilers Handle Position", "", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Brake Left Position", "", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
            m_scConnection.AddToDataDefinition(DEFINITIONS.PERIODIQUE, "Brake Right Position", "", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);

            // On associe notre structure à la définition simconnect
            m_scConnection.RegisterDataDefineStruct<DonneesAvion>(DEFINITIONS.PERIODIQUE);

            // On définie les fonctions callback pour les différents event de simconnect
            m_scConnection.OnRecvException += new SimConnect.RecvExceptionEventHandler(OnRecvException);
            m_scConnection.OnRecvQuit += new SimConnect.RecvQuitEventHandler(OnRecvQuit);
            m_scConnection.OnRecvSimobjectData += new SimConnect.RecvSimobjectDataEventHandler(OnRecvSimobjectData);

            //On programme la requete cyclique pour l'obtention des données de l'avion
            m_scConnection.RequestDataOnSimObject(DATA_REQUESTS.REQUEST_1, DEFINITIONS.PERIODIQUE, SimConnect.SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_PERIOD.SECOND, 0, 0, 0, 0);

        }

        private void CloseConnection()
        {
            m_scThreadRun = false; // stop the second thread
            if (m_scConnection != null)
            {
                m_scConnection.Dispose(); // delete the connection
                m_scConnection = null;
            }
            m_bConnected = false;
        }

        private void OnRecvException(SimConnect sender, SIMCONNECT_RECV_EXCEPTION data)
        {
            Console.WriteLine("Erreur SimConnect: " + data.dwException.ToString());
        }

        private void OnRecvSimobjectData(SimConnect sender, SIMCONNECT_RECV_SIMOBJECT_DATA data)
        {
            try
            {
                Donnees = (DonneesAvion)data.dwData[0];
            }
            catch (Exception e)
            {
                Console.WriteLine("OnRecvSimobjectData a échoué :" + e.Message);
            }
        }

        private void OnRecvQuit(SimConnect sender, SIMCONNECT_RECV data)
        {
            CloseConnection();
        }
    }
}
