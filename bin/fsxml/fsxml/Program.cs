﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using Microsoft.FlightSimulator.SimConnect;
using System.Runtime.InteropServices;

namespace fsxml
{
    class Program
    {
        static bool quit=false;
        static simulateur Sim;
        // Instance du serveur http
        static HttpListener listener;

        /// <summary>
        /// Boucle principale du serveur
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Console.WriteLine("FSXml démarré");

            // Instanciation du serveur http sur le port 8000
            listener = new HttpListener();
            listener.Prefixes.Add("http://+:8000/");
            Console.WriteLine("Listening..");
            listener.Start();
            Sim = new simulateur();
            // Boucle principale
            while (!quit)
            {
                // GetContext va bloquer jusqu'à obtention d'une requête. Le processus n'est pas bloqué en arrière plan pour
                // la gestion de la simconnect. 
                HttpListenerContext context = listener.GetContext();
                HttpListenerRequest request = context.Request;
                // On récupère la requête web qui nous est parvenue 
                HttpListenerResponse response = context.Response;
                // Analyse de la requête
                // Si la requete contien l'url /cmd en mode POST , il s'agit d'une commande vers le simulateur
                if ((request.RawUrl == "/cmd") && (request.HttpMethod == "POST") && request.HasEntityBody && request.ContentType == "application/json")
                {
                    //On Récupère le code json qui contien la commande
                    var sr = new System.IO.StreamReader(request.InputStream, request.ContentEncoding);
                    var code = sr.ReadToEnd();
                    Console.WriteLine(code);
                    var deserializer = new JavaScriptSerializer();
                    var json = deserializer.Deserialize<commande>(code);
                    Console.WriteLine(json.cmd + " : " + json.param);
                    json.cmd = json.cmd.ToUpper();
                    Byte[] Bytes = BitConverter.GetBytes(Convert.ToInt32(json.param));
                    UInt32 Param = BitConverter.ToUInt32(Bytes, 0);
                    // On envoie la commande à simconnect en parsant notre enumerateur pour retrouver l'id 
                    if (Sim.Transmit(json.cmd, Param))
                    {
                        response.StatusCode = 404;
                        response.StatusDescription = "Not Found";
                        Console.WriteLine(response.StatusCode.ToString() + response.StatusDescription);
                    }
                    response.Close();
                }
                // Sinon on test si il s'agit d'une requête GET avec l'url /getall pour récupérer les données fsx
                else if (request.RawUrl == "/getall")
                {
                    //Construction de la réponse en json
                    response.ContentType = "application/json";
                    var serializer = new JavaScriptSerializer();
                    string json = serializer.Serialize(Sim.Donnees);
                    byte[] buffer = System.Text.Encoding.UTF8.GetBytes(json);
                    var output = response.OutputStream;
                    output.Write(buffer, 0, buffer.Length);
                    output.Close();
                    response.Close();

                }
                // Sinon on retourn une erreur 404 au client
                else
                {
                    response.StatusCode = 404;
                    response.StatusDescription = "Not Found";
                    response.Close();
                }
            }
            listener.Stop();
        }
    }
}
