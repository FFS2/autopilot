var url="http://127.0.0.1:8000/cmd";
var url2="http://127.0.0.1:8000/getall";

exports.init = function(SARAH, SARAH2)
	{
		SARAH.speak("initialisation de lauto-pilote () terminé.");
	}﻿

function dump(obj)
	{
		var out = '';
		for (var i in obj) {out += i + "= " + obj[i] + "\n";}
		console.log(out);
	}


exports.action = function(data, callback, config, SARAH)
	{
		var tableau = new Object();	

		function fsx_master(commande,parametre,ok,pas_ok,callback_in,mod,encore,echec)
			{
				

				if (parametre==undefined)
					{
						var options = 	{
											'uri': url,
											'method': 'POST',
											'Headers' : 'Content-type: application/json',
											'json': {"cmd": commande},
										};
					}
				else
					{
						var options = 	{
									'uri': url,
									'method': 'POST',
									'Headers' : 'Content-type: application/json',
									'json': {"cmd": commande , "param": parametre},
								};
					}


				request=require('request');
				request(options, function (err, response, body)
					{
						if (!err && response.statusCode == 200)
							{
								if (mod=="simple"){callback({'tts': oui });}
								else{SARAH.speak(oui);}
							}
						else
							{
								if (mod=="simple"){callback({'tts': non });}
								else{SARAH.speak(non);}
							}


						if (mod=="simple") {}
						else if (mod=="inter"){}
						else if (mod=="fin"){callback({'tts': callback_in});}
					});
			}
		SARAH.play("plugins/autopilot/wav/Speech_sarah.wav");

		if (!data.dictation)
			{
				if (data.val=="la")
					{
						var Txt = new Array; 
						Txt[0] = "tout est fermé";
						Txt[1] = "je m'en auccupe";
						Txt[2] = "tout de suite";
						Txt[3] = "c'est fait";
						Choix = Math.floor(Math.random() * Txt.length); 
						callback({'tts': Txt[Choix]});
					}
				else if (data.val=="sim_connect_on")
					{
						var process = '%CD%/plugins/autopilot/bin/fsxml.exe';
						var exec = require('child_process').exec;
						var child =exec(process,
							function (error, stdout, stderr)
								{
									console.log(process);
									callback({'tts':"lancement de la passerelle"});
								});
					}
				else if (data.val=="fsx")
					{
						var process = '%CD%/plugins/autopilot/bin/fsx.bat';
						var exec = require('child_process').exec;
						var child =exec(process,
							function (error, stdout, stderr)
								{
									console.log(process);
									var Txt = new Array; 
										Txt[0] = "c'est partis pour un vol, serons nous seul";
										Txt[1] = "c'est partis pour un vol, dois-je prévenir les secours avant de partir";
										Txt[2] = "c'est partis pour un vol,  où allons nous";
									Choix = Math.floor(Math.random() * Txt.length); 
									callback({'tts': Txt[Choix]});
								});
					}
				else if (data.val=="fsx_all")
					{
						var process = '%CD%/plugins/autopilot/bin/fsx_all.bat';
						var exec = require('child_process').exec;
						var child =exec(process,
							function (error, stdout, stderr)
								{
									console.log(process);
									var Txt = new Array; 
										Txt[0] = "c'est partis pour un vol, serons nous seul";
										Txt[1] = "c'est partis pour un vol, dois-je prévenir les secours avant de partir";
										Txt[2] = "c'est partis pour un vol,  où allons nous";
									Choix = Math.floor(Math.random() * Txt.length); 
									callback({'tts': Txt[Choix]});
								});
					}
				else if (data.val=="fin_vol")
					{
						var process = '%CD%/plugins/autopilot/bin/fin_vol.bat';
						var exec = require('child_process').exec;
						var child =exec(process,
							function (error, stdout, stderr)
								{
									console.log(process);
									var Txt = new Array; 
										Txt[0] = "Vol en cours de fermeture";
										Txt[1] = "Es ce des morceaux de votre avion sur le seuille de piste";
										Txt[2] = "Pour une fois tous le monde n'est pas mort. Enfin mort de peur.";
									Choix = Math.floor(Math.random() * Txt.length); 
									callback({'tts': Txt[Choix]});
								});
					}
				else if (data.val=="reboot_fsx")
					{
						var process = '%CD%/plugins/autopilot/bin/re-fsx.bat';
						var exec = require('child_process').exec;
						var child =exec(process,
							function (error, stdout, stderr)
								{
									console.log(process);
									var Txt = new Array; 
										Txt[0] = "Redémarrage du systaime de vol en cours";
										Txt[1] = "Encore un soucis avec FSX monsieur?";
									Choix = Math.floor(Math.random() * Txt.length); 
									callback({'tts': Txt[Choix]});
								});
					}
				else if (data.val=="test_system")
					{
						var methode_to_c='none';
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_state(url2,callback,methode_to_c);
					}
				else if (data.val=="info_system")
					{
						var methode_to_c='web';
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_state(url2,callback,methode_to_c);
					}
				else if (data.val=="auto_on")
					{
						var parametre='none';
						var commande="AUTOPILOT_ON";
						var callback_commande="Autopilot_Master";
						var ok="Auto-pilote activée";
						var pas_ok="Pas de réponse de l'auto-pilote monsieur";
						var encore="l'autopilote est déja activé";
						var echec="monsieur l'autopilote ne veux pas s'activer";
						var etat=1;
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat,callback);
					}
				else if (data.val=="auto_off")
					{
						var parametre='none';
						var commande="AUTOPILOT_OFF";
						var callback_commande="Autopilot_Master";
						var ok="Auto-pilote désactivée";
						var pas_ok="Pas de réponse de l'auto-pilote monsieur";
						var encore="l'autopilote est déja désactivé";
						var echec="monsieur l'autopilote ne veux pas se désactiver";
						var etat=0;
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat,callback);
					}
				else if (data.val=="alt_select")
					{
						var parametre=data.parameters;
						var commande="AP_ALT_VAR_SET_ENGLISH";
						var callback_commande="Autopilot_Altitude_Lock_Var";
						var ok="altitude modifié à " +data.parameters+ " pieds ()";
						var pas_ok="pas de réponse du conservateur d'altitude monsieur";
						var encore="l'altitude est déjà réglé sur "+data.parameters+ " pieds ()";
						var echec="monsieur pas moyen de régler le conservateur d'altitude";
						var etat=data.parameters;
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat,callback);
					}
				else if (data.val=="hdg_select")
					{
						var parametre=data.parameters;
						var commande="HEADING_BUG_SET";
						var callback_commande="Autopilot_Heading_Lock_Dir";
						var ok="Cap modifié au " +data.parameters;
						var pas_ok="pas de réponse du conservateur de cap monsieur";
						var encore="le cap est déjà réglé sur "+data.parameters+ "";
						var echec="monsieur pas moyen de régler le conservateur de cap";
						var etat=data.parameters;
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat,callback);
					}
				else if (data.val=="crs_select")
					{
						var parametre=data.parameters;
						var commande="VOR1_SET";
						var ok="Course modifié au " +data.parameters;
						var pas_ok="pas de réponse du conservateur de course monsieur";
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="hdg_on")
					{
						var parametre=data.parameters;
						var commande="AP_HDG_HOLD_ON";
						var callback_commande="Autopilot_Heading_Lock";
						var ok="Conservateur de cap activée";
						var pas_ok="pas de réponse du conservateur de cap";
						var encore="le conservateur de cap est déjà activée";
						var echec="monsieur pas moyen d'activer le conservateur de cap";
						var etat=1;
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat,callback);
					}
				else if (data.val=="hdg_off")
					{
						var parametre=data.parameters;
						var commande="AP_HDG_HOLD_OFF";
						var callback_commande="Autopilot_Heading_Lock";
						var ok="Conservateur de cap désactivée";
						var pas_ok="pas de réponse du conservateur de cap";
						var encore="le conservateur de cap est déjà désactivée";
						var echec="monsieur pas moyen de désactiver le conservateur de cap";
						var etat=0;
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat,callback);
					}
				else if (data.val=="speed_select")
					{
						var parametre=data.parameters;
						var commande="AP_SPD_VAR_SET";
						var callback_commande="Autopilot_Airspeed_Hold_Var";
						var ok="Vitesse modifiée à " +data.parameters + "noeuds";
						var pas_ok="pas de réponse du conservateur de vitesse";
						var encore="la vitesse est déjà réglée sur "+data.parameters+ "noeuds";
						var echec="monsieur pas moyen de régler le conservateur de vitesse";
						var etat=data.parameters;
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat,callback);

					}
				else if (data.val=="alt_on")
					{
						var parametre='none';
						var commande="AP_ALT_HOLD_ON";
						var callback_commande="Autopilot_Altitude_Lock";
						var ok="Conservateur d'altitude activée";
						var pas_ok="pas de réponse du conservateur d'altitude";
						var encore="le conservateur d'altitude est déjà activée";
						var echec="monsieur pas moyen d'activer le conservateur d'altitude";
						var etat=1;
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat,callback);
					}
				else if (data.val=="alt_off")
					{

						var parametre='none';
						var commande="AP_ALT_HOLD_OFF";
						var callback_commande="Autopilot_Altitude_Lock";
						var ok="Conservateur d'altitude désactivée";
						var pas_ok="pas de réponse du conservateur d'altitude";
						var encore="le conservateur d'altitude est déjà désactivée";
						var echec="monsieur pas moyen de désactiver le conservateur d'altitude";
						var etat=0;
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat,callback);
					}
				else if (data.val=="nav_on")
					{
						var parametre='none';
						var commande="AP_NAV1_HOLD_ON";
						var callback_commande="Autopilot_Nav1_Lock";
						var ok="Navigation activée";
						var pas_ok="pas de réponse du circuit de navigation";
						var encore="la navigation est déjà activée";
						var echec="monsieur pas moyen d'activer la navigation";
						var etat=1;
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat,callback);
					}
				else if (data.val=="nav_off")
					{
						var parametre='none';
						var commande="AP_NAV1_HOLD_OFF";
						var callback_commande="Autopilot_Nav1_Lock";
						var ok="Navigation désactivée";
						var pas_ok="pas de réponse du circuit de navigation";
						var encore="la navigation est déjà désactivée";
						var echec="monsieur pas moyen de désactiver la navigation";
						var etat=0;
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat,callback);
					}
				else if (data.val=="app_on")
					{
						var parametre='none';
						var commande="AP_APR_HOLD_ON";
						var callback_commande="Autopilot_Approach_Hold";
						var ok="Approche activée";
						var pas_ok="pas de réponse du circuit d'approche";
						var encore="l'approche est déjà activée";
						var echec="monsieur pas moyen d'activer l'approche";
						var etat=1;
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat,callback);
					}
				else if (data.val=="app_off")
					{
						var parametre='none';
						var commande="AP_APR_HOLD_OFF";
						var callback_commande="Autopilot_Approach_Hold";
						var ok="Approche désactivée";
						var pas_ok="pas de réponse du circuit d'approche";
						var encore="l'approche est déjà désactivée";
						var echec="monsieur pas moyen de désactiver l'approche";
						var etat=1;
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat,callback);
					}
				else if (data.val=="nav1_select")
					{
						var integer=data.parameters;
						integer=integer.replace(",", ".");
						integer=integer*100;
						integer=parseInt(integer);
						
						if (integer>10800 && integer<11795)
							{
								var convert_bcd=require('./module/convert')
								var bcd = convert_bcd.to_bcd_radio(integer);
								
								var commande="NAV1_STBY_SET";
								var ok="Radio navigation réglé sur " +data.parameters;
								var pas_ok="pas de réponse de la radio nav";
								var parametre=bcd;

								sim_resquest = require('./module/request_sim');
								sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
							}
						else
							{
								callback({'tts': "Je n'ai pas compris la fréquence"});
							}
					}
				else if (data.val=="com1_select")
					{
						var integer=data.parameters;
						integer=integer.replace(",", ".");
						integer=integer*100;
						integer=parseInt(integer);
						
						if (integer>11800 && integer<13697)
							{
						
								var convert_bcd=require('./module/convert')
								var bcd = convert_bcd.to_bcd_radio(integer);
								
								var commande="COM_STBY_RADIO_SET";
								var ok="Radio communication réglé sur " +data.parameters;
								var pas_ok="pas de réponse de la radio com";
								var parametre=bcd;

								sim_resquest = require('./module/request_sim');
								sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
							}
						else
							{
								callback({'tts': "Je n'ai pas compris la fréquence"});
							}
					}
				else if (data.val=="switch_nav")
					{
						var parametre='none';
						var commande="NAV1_RADIO_SWAP";
						var ok="navigation commuté";
						var pas_ok="Pas de réponse de la radio nav";
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="switch_com")
					{
						var parametre='none';
						var commande="COM_STBY_RADIO_SWAP";
						var ok="communication commuté";
						var pas_ok="Pas de réponse de la radio com";
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="gear_up")
					{
						var parametre='none';
						var commande="GEAR_UP";
						var ok="Rentrée du train";
						var pas_ok="pas de réponse du train d'aterrissage";
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="gear_down")
					{
						var parametre='none';
						var commande="GEAR_DOWN";
						var ok="Sortie du train";
						var pas_ok="pas de réponse du train d'aterrissage";
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="spoil_on")
					{
						var parametre='none';
						var commande="SPOILERS_ON";
						var ok="Ouverture des aérofreins";
						var pas_ok="pas de réponse des aérofreins";
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="spoil_off")
					{
						var parametre='none';
						var commande="SPOILERS_OFF";
						var ok="Fermeture des aérofreins";
						var pas_ok="pas de réponse des aérofreins";
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="flaps_up")
					{
						var parametre='none';
						var commande="FLAPS_UP";
						var ok="Rentrée des volets";
						var pas_ok="pas de réponse des volets";
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="flaps_down")
					{
						var parametre='none';
						var commande="FLAPS_DOWN";
						var ok="Sortie compléte des volets";
						var pas_ok="pas de réponse des volets";
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="flaps_1")
					{
						var parametre='none';
						var commande="FLAPS_1";
						var ok="Sortie des volets";
						var pas_ok="pas de réponse des volets";
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="flaps_2")
					{
						var parametre='none';
						var commande="FLAPS_2";
						var ok="Sortie des volets";
						var pas_ok="pas de réponse des volets";
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="flaps_3")
					{
						var parametre='none';
						var commande="FLAPS_3";
						var ok="Sortie des volets";
						var pas_ok="pas de réponse des volets";
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="flaps_incr")
					{
						var parametre='none';
						var commande="FLAPS_INCR";
						var ok="Sortie de volet";
						var pas_ok="pas de réponse des volets";
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="flaps_decr")
					{
						var parametre='none';
						var commande="FLAPS_DECR";
						var ok="Rentré de volets";
						var pas_ok="pas de réponse des volet";
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="flaps_pos")
					{
						var integer=data.parameters;
						var commande="FLAPS_SET";
						var ok="Positionnements des volets à "+data.parameters+" pour cent";
						var pas_ok="pas de réponse des volet";
						var parametre=parseInt(16383/100*integer);
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="auto_start")
					{
						var parametre='none';
						var commande="ENGINE_AUTO_START";
						var ok="Lancement de la séquence de démarrage moteur";
						var pas_ok="pas de réponse de la gestion moteur";
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="auto_shutdown")
					{
						var parametre='none';
						var commande="ENGINE_AUTO_SHUTDOWN";
						var ok="Coupure des moteurs";
						var pas_ok="pas de réponse de la gestion moteur";
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="engine_power")
					{
						var integer=data.parameters;
						var commande="THROTTLE_SET";
						var ok="Puissance moteur à "+data.parameters+" pour cent";
						var pas_ok="pas de réponse des moteurs";
						var parametre=parseInt(16383/100*integer);
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="full_power")
					{
						var commande="THROTTLE_SET";
						var parametre='none';
						var ok="Pleine puissance";
						var pas_ok="pas de réponse des moteurs";
						var parametre=parseInt(16383);

						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="off_power")
					{
						var commande="THROTTLE_SET";
						var parametre='none';
						var ok="Moteurs au ralentit";
						var pas_ok="pas de réponse des moteurs";
						var parametre=0;

						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}
				else if (data.val=="throttle_on")
					{
						var parametre='none';
						var commande="AUTO_THROTTLE_ARM";
						var callback_commande="Autopilot_Throttle_Arm";
						var ok="Manette des gaz controlé";
						var pas_ok="pas de réponse de la manette des gaz";
						var encore="j'ai déja le controle de la manette des gaz";
						var echec="monsieur pas moyen de prendre le controle de la manette des gaz";
						var etat=1;
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat,callback);
					}
				else if (data.val=="throttle_off")
					{
						var parametre='none';
						var commande="AUTO_THROTTLE_ARM";
						var callback_commande="Autopilot_Throttle_Arm";
						var ok="Manette des gaz débrayée";
						var pas_ok="pas de réponse de la manette des gaz";
						var encore="vous avez déjà la manette des gaz sous votre controle";
						var echec="monsieur pas moyen de débrayer la manette des gaz";
						var etat=0;
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat,callback);
					}
				else if (data.val=="speed_on")
					{
						var parametre='none';
						var commande="AP_AIRSPEED_ON";
						var callback_commande="Autopilot_Airspeed_Hold";
						var ok="Conservateur de vitesse activée";
						var pas_ok="pas de réponse du conservateur de vitesse";
						var encore="Conservateur de vitesse est déjà activée";
						var echec="monsieur pas moyen d'activer le conservateur de vitesse";
						var etat=1;
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat,callback);
					}
				else if (data.val=="speed_off")
					{

						var parametre='none';
						var commande="AP_AIRSPEED_OFF";
						var callback_commande="Autopilot_Airspeed_Hold";
						var ok="Conservateur de vitesse désactivée";
						var pas_ok="pas de réponse du conservateur de vitesse";
						var encore="Conservateur de vitesse est déjà désactivée";
						var echec="monsieur pas moyen de désactiver le conservateur de vitesse";
						var etat=0;
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat,callback);

					}
				else if (data.val=="all_light_on")
					{
						var parametre='none';
						var commande="ALL_LIGHTS_TOGGLE";
						var ok="Es ce mieux ainsi?";
						var pas_ok="Je ne peux pas allumer l'éclairage";
						
						sim_resquest = require('./module/request_sim');
						sim_resquest.sim_req_bind(url,parametre,commande,ok,pas_ok,callback);
					}

				else if (data.val=="cowl_flaps_incr")
					{
						var parametre='none';
						var commande="INC_COWL_FLAPS";
						var ok="plus plus";
						var pas_ok="ouvre les toi même";
						
						sim_resquest = require('./module/request_sim');
						for (var i = 0; i < data.parameters; i++) {
							sim_resquest.sim_req_muted(url,parametre,commande,ok,pas_ok,callback);
						};

						//sim_resquest.sim_req_muted(url,parametre,commande,ok,pas_ok,callback);
						callback({'tts': "plus plus"});

					}
				else if (data.val=="cowl_flaps_decr")
					{
						var parametre='none';
						var commande="DEC_COWL_FLAPS";
						var ok="moins moins";
						var pas_ok="moins moins";
						
						sim_resquest = require('./module/request_sim');
						for (var i = 0; i < data.parameters; i++) {
							sim_resquest.sim_req_muted(url,parametre,commande,ok,pas_ok,callback);
						};
						callback({'tts': "moins moins"});
					}

				
				//partie non mise à jours
				
				////
				
				///
				
				
				/*else if (data.val=="emergency_stop")
					{
						var commande="THROTTLE_SET";
						var oui="Inverseur à pleine puissance";
						var non="pas de réponse des moteurs";
						
						var mod="inter";
						var parametre=-16383;

						fsx_master(commande,parametre,oui,non,callback_in,mod,url);

						var commande="FLAPS_DOWN";
						var oui="Volets plein ouverture";
						var non="pas de réponse des volets";
						
						var mod="inter";
						var parametre=-null;

						fsx_master(commande,parametre,oui,non,callback_in,mod,url);

						var commande="SPOILERS_ON";
						var oui="Ouverture des aérofreins";
						var non="pas de réponse des aérofreins";
						
						var mod="fin";
						

						fsx_master(commande,parametre,oui,non,callback_in,mod,url);

						var commande="PARKING_BRAKES";
						var oui="freinage sur les roue";
						var non="pas de réponse des freins";
						var callback_in="freinage d'urgence activé";
						var mod="fin";
						

						fsx_master(commande,parametre,oui,non,callback_in,mod,url);
					}*/
				else{return callback({'tts': "Je ne comprends pas"});}
			}
		else
			{}

	}

